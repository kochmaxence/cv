var CurriculumVitae = function() {
    
    this.informations = {
        nom: "Koch Maxence",
        age: 23,
        gsm: "0477 06 35 39",
        adresse: "avenue de Marsan 13, 1420 BRAINE-L'ALLEUD",
        mail: "koch.maxence@gmail.com",
        emploiActuel: "Dessinateur technique / Programmation robotique industrielle / Administrateur Réseaux"
    };
    
    this.connaissances = {
        langages: [
            "Java", "C#.NET", "C++", 
            "PHP", "JavaScript", "HTML5", "CSS3"
            ],
        backends: [
            "Apache", "nginX", 
            "NodeJS", 
            "MySQL", "SQLite", "MongoDB"
            ],
        frameworks: [
            "CodeIgniter", "FuelPHP", "Laravel", 
            "ExpressJS", "MeteorJS",
            "jQuery", "AngularJS", "BackboneJS",
            "Bootstrap", "Kube"
            ],
        outils: ["Visual Studio", "Eclipse", "Cloud9", "Git", "Windows/Linux (Debian)", "Google", "StackOverflow"]
    };
    
};

module.exports = new CurriculumVitae();

